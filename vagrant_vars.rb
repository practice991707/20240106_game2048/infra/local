# variables for vagrant

module Variables
  $VM_SPECS = {
    'game' => {
#      'ram' => 768,
#      'cpu' => 1,
      'qty' => 3,
      'script' => './scripts/update-dns.sh'
    },
    'lb' => {
      'ram' => 768,
      'cpu' => 1,
      'qty' => 3,
      'script' => './scripts/update-dns.sh'
    },
  }

  # Default CPU for VM
    VM_DEFAULT_CPU = 1
  # Default memory for VM
    VM_DEFAULT_RAM = 2048
  # Postfix for VMs name
    VM_POSTFIX=".deus.corp"
  # version of VM
    VM_BOX = "ubuntu/jammy64"
  # network type for project ("private" or "public")
    NETWORK_TYPE = "private"
  # ip subnet: first three octets with delimiters
    IP_RANGE = "192.168.56" # /24
  # "true" if you need ansible provisioning in this project
    NEED_PROVISIONING = true
  # path for generated inventory
    GENERATED_ANSIBLE_INVENTORY_PATH = "../ansible/inventory/dev/hosts"
  # playbook location
    ANSIBLE_PLAYBOOK = "../ansible/playbook.yml"
  # playbook location
    ANSIBLE_REQUIREMENTS = "../ansible/requirements.yml"
  # playbook location
    ANSIBLE_CONFIG = "../ansible/ansible.cfg"
  # ansible user
    ANSIBLE_USER = "vagrant"
  # Variable to be added in all:vars sections in generated inventory. one line with \n at the end
    ANSIBLE_INVENTORY_STRING = "#key1=value1\n#key2=value2\n"
    
end
    