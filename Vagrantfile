#Set addres for boxes
ENV['VAGRANT_SERVER_URL']='https://vagant.elab.pro'

# -*- mode: ruby -*-
# vi: set ft=ruby :
require File.join(File.dirname(__FILE__), 'vagrant_vars.rb')
include Variables

$total_qty = $VM_SPECS.sum { |key, value| value.fetch('qty', 1).to_i }

####################### define standard virtual maschines parameters #######################
def standard_vm_create(config, hostname, ip_address, vm_name)
  config.vm.define hostname do |host|
    host.vm.provider "virtualbox" do |vb|
      vb.name = hostname
      if ($VM_SPECS[vm_name]['cpu'].nil?)
        vb.cpus = VM_DEFAULT_CPU 
      else 
        vb.cpus = $VM_SPECS[vm_name]['cpu'].to_i 
      end
      if ($VM_SPECS[vm_name]['ram'].nil?)
        vb.memory = VM_DEFAULT_RAM
      else 
        vb.memory = $VM_SPECS[vm_name]['ram'].to_i
      end
      vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ] #Disconnect com-port for virtualbox support
    end
    host.vm.hostname = hostname
    host.vm.network "#{NETWORK_TYPE}_network", ip: ip_address
    host.vm.provision "shell" do |s|
        ssh_pub_key = File.readlines("./scripts/pubkey.pub").first.strip
        s.inline = <<-SHELL
        # setting additional SSH keys for user vagrant
          echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
        SHELL
    end
    if !($VM_SPECS[vm_name]['script'].nil?) && !($VM_SPECS[vm_name]['script'].empty?)
      host.vm.provision "shell", path: $VM_SPECS[vm_name]['script']
    end
  end
end

################### create virtual maschines with additional parameters ####################
Vagrant.configure("2") do |config|
  config.vm.box = VM_BOX
  config.vm.box_check_update = false
  config.vm.boot_timeout = 900
  config.vm.synced_folder ".", "/vagrant", disabled: true
  count = 0
  # Create groups to be used in ansible inventory
  groups = {"all" => []}

  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = false
  end

$ANSIBLE_INVENTORY = ""
$ANSIBLE_INVENTORY_ALL = "\n[all]\n"
$ANSIBLE_INVENTORY_GROUP = ""
$ANSIBLE_INVENTORY_APPEND = "\n[all:vars]\n#Extra vars\n" + ANSIBLE_INVENTORY_STRING



  $VM_SPECS.each do |key, value|

    # Generate group in inventory
    $ANSIBLE_INVENTORY_GROUP += "\n[#{key}]\n"

    qty = value.fetch('qty', 1).to_i
      (1..qty).each do |server_index|
        hostname = "#{key}#{"%02d" % server_index}#{VM_POSTFIX}"
        if ! groups.has_key?("#{key}")
          groups["#{key}"] = ["#{hostname}"]
        else
          groups["#{key}"].push("#{hostname}")
        end
        groups["all"].push("#{hostname}")
        # Add hostname to inventory 
        $ANSIBLE_INVENTORY_GROUP += "#{hostname}\n"
        $ANSIBLE_INVENTORY_ALL += "#{hostname}\n"
        $ANSIBLE_INVENTORY += "#{hostname} ansible_host=#{IP_RANGE}.#{10+count} ansible_user=#{ANSIBLE_USER}\n"
        # Generate VMs
        standard_vm_create(config, "#{hostname}", "#{IP_RANGE}.#{10+count}", key)
        # run ansible provision
        if (NEED_PROVISIONING == true) && ( count == $total_qty - 1 )
          config.vm.define "#{hostname}" do |host|
            host.vm.provision "ansible" do |ansible|
              # Disable default limit to connect to all the machines
              ansible.limit = "all"
              ansible.playbook = "#{ANSIBLE_PLAYBOOK}"
              if !(ANSIBLE_REQUIREMENTS.nil?) && !(ANSIBLE_REQUIREMENTS.empty?)
                ansible.galaxy_role_file = "#{ANSIBLE_REQUIREMENTS}"
              end
              if !(ANSIBLE_CONFIG.nil?) && !(ANSIBLE_CONFIG.empty?)
                ansible.config_file = "#{ANSIBLE_CONFIG}"
              end              
              ansible.verbose = 'v'
              ansible.inventory_path = "#{GENERATED_ANSIBLE_INVENTORY_PATH}"
              ansible.groups = groups #for vagrant-generated inventory
            end
          end
        end 
        # Increase counter for network IP
        count = count + 1
      end

  # generate ANSIBLE_INVENTORY
  end
  if !(GENERATED_ANSIBLE_INVENTORY_PATH.nil?) && !(GENERATED_ANSIBLE_INVENTORY_PATH.empty?)
    unless File.exist?( GENERATED_ANSIBLE_INVENTORY_PATH )
      File.write( GENERATED_ANSIBLE_INVENTORY_PATH , $ANSIBLE_INVENTORY + $ANSIBLE_INVENTORY_ALL + $ANSIBLE_INVENTORY_GROUP + $ANSIBLE_INVENTORY_APPEND)
  end 
end
end