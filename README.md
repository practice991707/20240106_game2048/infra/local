# Vagrantfile для разворачивания тествого проекта


* Vagrantfile протестирован на Virtualbox 7.0.14 с box "ubuntu/jammy64". Для работы требуются плагины virtualbox_WSL2 и vagrant-hostmanager
```
vagrant plugin install --plugin-clean-sources --plugin-source https://rubygems.org  virtualbox_WSL2
vagrant plugin install --plugin-clean-sources --plugin-source https://rubygems.org  vagrant-hostmanager
```

* Необходимые настройки генерируются через файл с переменными vagrant_vars.rb. 

* Для настройки тестового окружения необходимо сформировать виртуальных машин $VM_SPECS. В случае отстутствия настроек оперативной памяти (ram) и ядер процессора (cpu) будут использоваться значения по умолчанию из переменных $VM_DEFAULT_CPU и $VM_DEFAULT_RAM. В случае отстутствия настроек количества виртуальных машин (qty) будет создаваться по 1 экземпляру. Настройка может производиться через файл скрипта в переменной script.

* Дополнительно настроено добавление файла ./scripts/pubkey.pub для пользователя vagrant при создании VM.

* Если задана переменная $GENERATED_ANSIBLE_INVENTORY_PATH будет сгенерирован инвентори файл для Ansible. Дополнительные опции в него можно передать через перменную $ANSIBLE_INVENTORY_STRING.

* Если указана переменная $NEED_PROVISIONING=true, то будет запущен плейбук из переменной $ANSIBLE_PLAYBOOK. Зависимости для него можно указать в переменной  $ANSIBLE_REQUIREMENTS. Конфиг для Ansible в переменной $ANSIBLE_CONFIG.

License
-------

GPLv3

Author Information
------------------

* Kirill Fedorov (tg:fedrr) as a challege for ClassBook #240106 project https://deusops.com/classbook